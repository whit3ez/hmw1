def fibonacci(n):
    if n <= 1:
        return n
    else:
        return (fibonacci(n-1) + fibonacci(n-2))

n = int(input())
if n < 0:
    raise ValueError("n неотрицательное целое")
print(fibonacci(n))

