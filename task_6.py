from sympy import isprime
n = int(input())
if n < 0:
    raise ValueError('Число должно быть >= 0')
else:
    print("Ни простое ни составное") if n in (0, 1) else print("Простое") if isprime(n) else print("Составное")
